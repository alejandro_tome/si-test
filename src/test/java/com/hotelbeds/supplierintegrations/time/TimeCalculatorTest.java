package com.hotelbeds.supplierintegrations.time;

import org.junit.Test;

import java.time.format.DateTimeParseException;

import static org.junit.Assert.assertEquals;

public class TimeCalculatorTest {

    private static final String RFC_2822_DATE_FORMAT_FROM = "Thu, 20 Dec 2018 16:01:57 +0200";
    private static final String RFC_2822_DATE_FORMAT_TO_3_MINUTES_AFTER = "Thu, 20 Dec 2018 16:04:37 +0200";
    private static final String RFC_2822_DATE_FORMAT_TO_1_DAY_3_MINUTES_AFTER = "Fri, 21 Dec 2018 16:04:59 +0200";
    private static final String RFC_2822_DATE_FORMAT_TO_3_MINUTES_AFTER_DIFFERENT_TIME_ZONE = "Thu, 20 Dec 2018 15:04:57 +0100";
    private static final long MINUTES_PER_DAY = 24L * 60L;

    @Test
    public void testThreeMinutesDifference() throws Exception {
        assertEquals(2L, TimeCalculator.calculateNumberOfMinutesBetween(RFC_2822_DATE_FORMAT_FROM, RFC_2822_DATE_FORMAT_TO_3_MINUTES_AFTER));
    }

    @Test
    public void testInverseOrderMinusTwoMinutesDifference() throws Exception {
        assertEquals(-2L, TimeCalculator.calculateNumberOfMinutesBetween(RFC_2822_DATE_FORMAT_TO_3_MINUTES_AFTER, RFC_2822_DATE_FORMAT_FROM));
    }

    @Test
    public void testOneDayAndThreeMinutesDifference() throws Exception {

        assertEquals(MINUTES_PER_DAY + 3L, TimeCalculator.calculateNumberOfMinutesBetween(RFC_2822_DATE_FORMAT_FROM, RFC_2822_DATE_FORMAT_TO_1_DAY_3_MINUTES_AFTER));
    }

    @Test
    public void testThreeMinutesDifferenceInDifferentTimeZone() throws Exception {
        assertEquals(3L, TimeCalculator.calculateNumberOfMinutesBetween(RFC_2822_DATE_FORMAT_FROM, RFC_2822_DATE_FORMAT_TO_3_MINUTES_AFTER_DIFFERENT_TIME_ZONE));
    }

    @Test(expected = DateTimeParseException.class)
    public void shouldThrowsDateTimeParseException() throws Exception {
        assertEquals(3L, TimeCalculator.calculateNumberOfMinutesBetween(RFC_2822_DATE_FORMAT_FROM, "20 Dec 2018 21:09:52"));
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowsNullPointerException() throws Exception {
        assertEquals(3L, TimeCalculator.calculateNumberOfMinutesBetween(RFC_2822_DATE_FORMAT_FROM, null));
    }
}
