package com.hotelbeds.supplierintegrations.hackertest.converter;

import com.hotelbeds.supplierintegrations.hackertest.model.Action;
import com.hotelbeds.supplierintegrations.hackertest.model.LogLine;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class StringToLogLineConverterTest {

    private static final String IP = "80.238.9.179";
    private static final long DATE = 133612947L;
    private static final Action ACTION_FAILURE = Action.SIGNIN_FAILURE;
    private static final String USERNAME = "Will.Smith";
    private static final String LINE = IP + "," + DATE + "," + ACTION_FAILURE + "," + USERNAME;
    private static final String LINE_WITH_ERRORS = IP + "," + DATE + "," + USERNAME;
    private static final String LINE_WITH_PARSE_ERRORS = IP + ", ERROR," + ACTION_FAILURE + "," + USERNAME;

    private final static LogLine LOG_LINE = LogLine.builder()
                                                .ip(IP)
                                                .date(DATE)
                                                .action(ACTION_FAILURE)
                                                .username(USERNAME)
                                                .build();

    private final StringToLogLineConverter stringToLogLineConverter = new StringToLogLineConverter();

    @Test
    public void testConvert() throws Exception {
        assertEquals(LOG_LINE, stringToLogLineConverter.convert(LINE));
    }

    @Test
    public void testConvertLineWhithErrors() throws Exception {
        assertNull(stringToLogLineConverter.convert(LINE_WITH_ERRORS));
    }

    @Test(expected = NumberFormatException.class)
    public void testConvertLineWhithParseError() throws Exception {
        assertNull(stringToLogLineConverter.convert(LINE_WITH_PARSE_ERRORS));
    }

    @Test(expected = NullPointerException.class)
    public void testConvertLineNull() throws Exception {
        assertNull(stringToLogLineConverter.convert(null));
    }


}
