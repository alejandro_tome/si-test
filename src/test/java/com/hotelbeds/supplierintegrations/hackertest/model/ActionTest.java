package com.hotelbeds.supplierintegrations.hackertest.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ActionTest {

    @Test
    public void testToString() throws Exception {
        assertEquals("SIGNIN_SUCCESS", Action.SIGNIN_SUCCESS.toString());
        assertEquals("SIGNIN_FAILURE", Action.SIGNIN_FAILURE.toString());
    }
}
