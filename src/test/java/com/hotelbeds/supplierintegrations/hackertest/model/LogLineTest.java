package com.hotelbeds.supplierintegrations.hackertest.model;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LogLineTest {

    private static final String IP = "80.238.9.179";
    private static final long DATE = 133612947L;
    private static final Action ACTION_FAILURE = Action.SIGNIN_FAILURE;
    private static final String USERNAME = "Will.Smith";

    private final static LogLine LOG_LINE = LogLine.builder()
                                        .ip(IP)
                                        .date(DATE)
                                        .action(ACTION_FAILURE)
                                        .username(USERNAME)
                                        .build();

    @Test
    public void testBuilder() throws Exception {
        assertEquals(IP, LOG_LINE.getIp());
        assertEquals(DATE, LOG_LINE.getDate());
        assertEquals(ACTION_FAILURE, LOG_LINE.getAction());
        assertEquals(USERNAME, LOG_LINE.getUsername());
    }

    @Test
    public void testEqualsAndHashCode() throws Exception {
        EqualsVerifier.forClass(LogLine.class).verify();
    }

}
