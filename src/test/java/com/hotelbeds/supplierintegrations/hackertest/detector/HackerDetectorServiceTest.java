package com.hotelbeds.supplierintegrations.hackertest.detector;

import com.hotelbeds.supplierintegrations.hackertest.converter.StringToLogLineConverter;
import com.hotelbeds.supplierintegrations.hackertest.model.Action;
import com.hotelbeds.supplierintegrations.hackertest.model.LogLine;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class HackerDetectorServiceTest {
    private static final String IP = "80.238.9.179";
    private static final long MILLISECONDS_PER_MINUTE = 60000L;
    private static final long DATE = 133612947L;
    private static final long DATE_2 = 133612948L;
    private static final long DATE_3 = 133612949L;
    private static final long DATE_4 = 133612950L;
    private static final long DATE_5 = 133612951L;
    private static final long DATE_6 = 5 * MILLISECONDS_PER_MINUTE + DATE;
    private static final Action ACTION_SUCCESS = Action.SIGNIN_SUCCESS;
    private static final Action ACTION_FAILURE = Action.SIGNIN_FAILURE;
    private static final String USERNAME = "Will.Smith";
    private static final String LINE_SUCCESS = IP + "," + DATE_5 + "," + ACTION_SUCCESS + "," + USERNAME;
    private static final String LINE_FAILURE = IP + "," + DATE + "," + ACTION_FAILURE + "," + USERNAME;
    private static final String LINE_FAILURE_2 = IP + "," + DATE_2 + "," + ACTION_FAILURE + "," + USERNAME;
    private static final String LINE_FAILURE_3 = IP + "," + DATE_3 + "," + ACTION_FAILURE + "," + USERNAME;
    private static final String LINE_FAILURE_4 = IP + "," + DATE_4 + "," + ACTION_FAILURE + "," + USERNAME;
    private static final String LINE_FAILURE_5 = IP + "," + DATE_5 + "," + ACTION_FAILURE + "," + USERNAME;
    private static final String LINE_FAILURE_FIVE_MINUTES_PASSED = IP + "," + DATE_6 + "," + ACTION_FAILURE + "," + USERNAME;
    private static final String LINE_WITH_ERROR = IP + "," + DATE_5 + "," + ACTION_FAILURE;
    private static final LogLine LOG_LINE_SUCCESS = LogLine.builder().ip(IP).date(DATE_5).action(ACTION_SUCCESS).username(USERNAME).build();
    private static final LogLine LOG_LINE_FAILURE = LogLine.builder().ip(IP).date(DATE).action(ACTION_FAILURE).username(USERNAME).build();
    private static final LogLine LOG_LINE_FAILURE_2 = LogLine.builder().ip(IP).date(DATE_2).action(ACTION_FAILURE).username(USERNAME).build();
    private static final LogLine LOG_LINE_FAILURE_3 = LogLine.builder().ip(IP).date(DATE_3).action(ACTION_FAILURE).username(USERNAME).build();
    private static final LogLine LOG_LINE_FAILURE_4 = LogLine.builder().ip(IP).date(DATE_4).action(ACTION_FAILURE).username(USERNAME).build();
    private static final LogLine LOG_LINE_FAILURE_5 = LogLine.builder().ip(IP).date(DATE_5).action(ACTION_FAILURE).username(USERNAME).build();
    private static final LogLine LOG_LINE_FAILURE_FIVE_MINUTES_PASSED = LogLine.builder().ip(IP).date(DATE_6).action(ACTION_FAILURE).username(USERNAME).build();


    @Mock
    private StringToLogLineConverter stringToLogLineConverter;

    @InjectMocks
    private HackerDetectorService hackerDetectorService;

    @Test
    public void testParseLineReturnNullWhenActionSuccess() throws Exception {
        givenStringToLogLineConverter(LINE_SUCCESS, LOG_LINE_SUCCESS);
        assertNull(hackerDetectorService.parseLine(LINE_SUCCESS));
    }

    @Test
    public void testParseLineReturnNullWhenLineWithError() throws Exception {
        givenStringToLogLineConverter(LINE_WITH_ERROR, null);
        assertNull(hackerDetectorService.parseLine(LINE_SUCCESS));
    }

    @Test
    public void testParseLineReturnNullWhenActionSuccessAfterFourFailures() throws Exception {

        givenStringToLogLineConverter(LINE_FAILURE, LOG_LINE_FAILURE);
        givenStringToLogLineConverter(LINE_FAILURE_2, LOG_LINE_FAILURE_2);
        givenStringToLogLineConverter(LINE_FAILURE_3, LOG_LINE_FAILURE_3);
        givenStringToLogLineConverter(LINE_FAILURE_4, LOG_LINE_FAILURE_4);
        givenStringToLogLineConverter(LINE_SUCCESS, LOG_LINE_SUCCESS);

        assertNull(hackerDetectorService.parseLine(LINE_FAILURE));
        assertNull(hackerDetectorService.parseLine(LINE_FAILURE_2));
        assertNull(hackerDetectorService.parseLine(LINE_FAILURE_3));
        assertNull(hackerDetectorService.parseLine(LINE_FAILURE_4));
        assertNull(hackerDetectorService.parseLine(LINE_SUCCESS));
    }

    @Test
    public void testParseLineReturnIPAfterFiveFailuresInLessThanFiveMinutes() throws Exception {
        givenStringToLogLineConverter(LINE_FAILURE, LOG_LINE_FAILURE);
        givenStringToLogLineConverter(LINE_FAILURE_2, LOG_LINE_FAILURE_2);
        givenStringToLogLineConverter(LINE_FAILURE_3, LOG_LINE_FAILURE_3);
        givenStringToLogLineConverter(LINE_FAILURE_4, LOG_LINE_FAILURE_4);
        givenStringToLogLineConverter(LINE_FAILURE_5, LOG_LINE_FAILURE_5);

        assertNull(hackerDetectorService.parseLine(LINE_FAILURE));
        assertNull(hackerDetectorService.parseLine(LINE_FAILURE_2));
        assertNull(hackerDetectorService.parseLine(LINE_FAILURE_3));
        assertNull(hackerDetectorService.parseLine(LINE_FAILURE_4));
        assertEquals(IP, hackerDetectorService.parseLine(LINE_FAILURE_5));
    }

    @Test
    public void testParseLineReturnNullAfterFiveFailuresInMoreThanFiveMinutes() throws Exception {
        givenStringToLogLineConverter(LINE_FAILURE, LOG_LINE_FAILURE);
        givenStringToLogLineConverter(LINE_FAILURE_2, LOG_LINE_FAILURE_2);
        givenStringToLogLineConverter(LINE_FAILURE_3, LOG_LINE_FAILURE_3);
        givenStringToLogLineConverter(LINE_FAILURE_4, LOG_LINE_FAILURE_4);
        givenStringToLogLineConverter(LINE_FAILURE_FIVE_MINUTES_PASSED, LOG_LINE_FAILURE_FIVE_MINUTES_PASSED);

        assertNull(hackerDetectorService.parseLine(LINE_FAILURE));
        assertNull(hackerDetectorService.parseLine(LINE_FAILURE_2));
        assertNull(hackerDetectorService.parseLine(LINE_FAILURE_3));
        assertNull(hackerDetectorService.parseLine(LINE_FAILURE_4));
        assertNull(hackerDetectorService.parseLine(LINE_FAILURE_FIVE_MINUTES_PASSED));
    }

    private void givenStringToLogLineConverter(final String line, final LogLine logLine) {
        given(stringToLogLineConverter.convert(line)).willReturn(logLine);
    }
}
