package com.hotelbeds.supplierintegrations.hackertest.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Builder
@Value
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class LogLine {
    String ip;
    long date;
    Action action;
    String username;
}
