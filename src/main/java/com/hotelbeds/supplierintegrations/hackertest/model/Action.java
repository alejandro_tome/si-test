package com.hotelbeds.supplierintegrations.hackertest.model;

public enum Action {
    SIGNIN_SUCCESS("SIGNIN_SUCCESS"),
    SIGNIN_FAILURE("SIGNIN_FAILURE");

    private final String name;

    Action(final String action) {
        name = action;
    }

    public String toString() {
        return this.name;
    }
}
