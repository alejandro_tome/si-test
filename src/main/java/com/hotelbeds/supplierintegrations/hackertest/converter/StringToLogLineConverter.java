package com.hotelbeds.supplierintegrations.hackertest.converter;

import com.hotelbeds.supplierintegrations.hackertest.model.Action;
import com.hotelbeds.supplierintegrations.hackertest.model.LogLine;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class StringToLogLineConverter implements Converter<String, LogLine> {

    private static final int ITEMS_PER_LOG_LINE = 4;
    private static final int IP_INDEX = 0;
    private static final int DATE_INDEX = 1;
    private static final int ACTION_INDEX = 2;
    private static final int USER_NAME_INDEX = 3;
    private static final String SPLIT_REGEX = ",";

    @Override
    public LogLine convert(final String line) {

        final List<String> lineItems = Stream.of(line.split(SPLIT_REGEX))
                .collect(Collectors.toList());

        return lineItems.size() == ITEMS_PER_LOG_LINE
                ? LogLine.builder()
                .ip(lineItems.get(IP_INDEX))
                .date(Long.parseLong(lineItems.get(DATE_INDEX)))
                .action(Enum.valueOf(Action.class, lineItems.get(ACTION_INDEX)))
                .username(lineItems.get(USER_NAME_INDEX))
                .build()
                : null;
    }

}
