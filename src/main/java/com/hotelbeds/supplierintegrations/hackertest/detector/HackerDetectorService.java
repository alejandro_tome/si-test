package com.hotelbeds.supplierintegrations.hackertest.detector;

import com.hotelbeds.supplierintegrations.hackertest.converter.StringToLogLineConverter;
import com.hotelbeds.supplierintegrations.hackertest.model.Action;
import com.hotelbeds.supplierintegrations.hackertest.model.LogLine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class HackerDetectorService implements HackerDetector {

    private static final long MILLISECONDS_PER_MINUTE = 60000L;
    private final Map<String, List<Long>> signInFailures;
    private final StringToLogLineConverter stringToLogLineConverter;

    @Autowired
    public HackerDetectorService(final StringToLogLineConverter stringToLogLineConverter) {
        this.stringToLogLineConverter = stringToLogLineConverter;
        this.signInFailures = new HashMap<>();
    }

    @Override
    public String parseLine(final String line) {
        final Optional<LogLine> logLine = Optional.ofNullable(this.stringToLogLineConverter.convert(line));

        return logLine
                   .map(this::suspiciousActivityIdentified)
                   .filter(identified -> identified)
                   .flatMap(identified -> logLine.map(LogLine::getIp))
                   .orElse(null);
    }

    private boolean suspiciousActivityIdentified(final LogLine logLine) {
        return this.updateAndReturnSignInFailures(logLine).size() >= 5;
    }

    private List<Long> updateAndReturnSignInFailures(final LogLine logLine) {
        if (Action.SIGNIN_SUCCESS == logLine.getAction()) {
            this.signInFailures.remove(logLine.getIp());
            return Collections.emptyList();
        } else {
            final List<Long> failureDates = obtainFailureDatesInFiveMinutesPeriod(this.signInFailures.get(logLine.getIp()), logLine.getDate());
            failureDates.add(logLine.getDate());
            this.signInFailures.put(logLine.getIp(), Collections.unmodifiableList(failureDates));
            return failureDates;
        }
    }

    private List<Long> obtainFailureDatesInFiveMinutesPeriod(final List<Long> signInFailureDates, final long date) {
        return signInFailureDates != null
                   ? signInFailureDates.stream()
                         .filter(signInFailureDate -> this.differenceInMinutesBetween(date, signInFailureDate) < 5)
                         .collect(Collectors.toList())
                   : new ArrayList<>();
    }

    private long differenceInMinutesBetween(final long epoch1, final long epoch2) {
        return BigDecimal.valueOf(epoch1 - epoch2).divide(BigDecimal.valueOf(MILLISECONDS_PER_MINUTE), RoundingMode.DOWN).longValue();
    }


}
