package com.hotelbeds.supplierintegrations.time;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Locale;

public class TimeCalculator {

    private static final DateTimeFormatter RFC_2822_DATE_FORMAT = DateTimeFormatter.ofPattern("EEE, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH);

    public TimeCalculator() throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    public static long calculateNumberOfMinutesBetween(final String time1, final String time2) {

        final ZonedDateTime zonedDateTime1 = ZonedDateTime.parse(time1, RFC_2822_DATE_FORMAT);
        final ZonedDateTime zonedDateTime2 = ZonedDateTime.parse(time2, RFC_2822_DATE_FORMAT);
        return ChronoUnit.MINUTES.between(zonedDateTime1, zonedDateTime2);
    }

}
